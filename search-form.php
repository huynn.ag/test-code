<li class="all-common-search-li">
    <a href="#" class="btn-search"><i class="fa fa-search"></i></a>
    <div class="modal-search all-common-search">
        <form>
            <div class="input-group">
              <input type="text" class="" placeholder="Tìm Kiếm">
              <span class="input-group-btn">
                <button class="btn btn-secondary" type="button"><i class="fa fa-search" aria-hidden="true"></i></button>
              </span>
            </div>
        </form>
    </div>
</li>
<script type="text/javascript">
// xu ly sticky-menu
$(window).scroll(navSlide);
function navSlide() {
    var scroll_top = $(window).scrollTop();

    if (scroll_top >= 160) { // the detection!
        $('body').addClass('is-sticky');
        $('.nw-logo').css("display", "none");
        
        $('.navigation .visible-sm .logo-responsive').show();
        $('#navigation-bar').addClass('animated fadeInDown').removeClass('fadeInUp');
    } else if(scroll_top <= 90) {
        $('body').removeClass('is-sticky');
        $('.navigation .visible-sm .logo-responsive').hide();
        $('.nw-logo').css("display", "block");
        $('#navigation-bar').removeClass('fadeInDown');

    }
}
// xu ly button search
$(window).click(function() {
  $('.modal-search').removeClass('is-show');
});
$('.all-common-search-li').click(function (event) {
  event.stopPropagation();
});
$('.all-common-search-li a').click(function (event) {

 $('.modal-search').addClass('is-show');
 return false;

});

</script>

<style type="text/css">
.modal-search{
	display: none;
}
.modal-search.is-show{
	position: absolute;
	display: block !important;
	right: 5%;
	padding: 5px;
	background-color: rgba(255, 255, 255, 0.3);
	border-radius: 5px;

    
}
.modal-search form input{
	background: #051A44!important;
	position: relative;

}
.all-common-search-li .all-common-search form button{
	padding: 7px 11px!important;
}
@media ( min-width: 768px ) and ( max-width: 991px ) {
	.modal-search{
		display: block!important;
	}
	.btn-search{
		display: none!important;
	}
}
@media ( max-width: 767px ) {
	.all-common-search-li a .fa{
		display: none!important;
	}
	.all-common-search-li .all-common-search {
		display: block!important;

	}
	.all-common-search-li .all-common-search form input{
		background: rgba(0, 0, 0, 0.3);
    	color: #fff;
	}
	.all-common-search-li{
		margin-right: 0px;
	}
}
	
</style>
       